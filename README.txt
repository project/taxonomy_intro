README.txt for taxonomy_intro.module

Description
-----------
The taxonomy_intro module simply enables you to display an introduction at taxonomy term pages where normally only the list of nodes related to this term is shown.
The modules brings up a new field 'introduction' in taxonomy term forms. 'description' should not be used for the introduction because it's shown as link title with taxonomy links.

Installation
------------
Upload the complete folder to your drupal sites modules folder, visit admin/build/modules to enable the Module!

Usage
-----
If you want to show an introduction at term pages, leave your message at the 'introduction' field in the 'Edit term' form. If you don't want to show an intro, leave the field blank. You also can select an input format for your introduction.